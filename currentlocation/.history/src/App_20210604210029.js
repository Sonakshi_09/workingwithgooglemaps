import React, { Component } from "react";
// import logo from './logo.svg';
import "./App.css";
// import Greet from './component/Greet'
// import Home from './component/home'
// import About from './component/about'
// import Contact from '.component/contact'
// import ButtonExampleAnimated from '.src/component/button'
import ImageExampleImage from "./component/ImageExampleImage";
import Login from "./component/login";
import Logout from "./component/logout";
// import { useHistory } from "react-router-dom";
import Main from "./component/Main";
import SearchNearBy from "./component/SearchNearBy";
import ChooseOption from "./component/chooseOption";
import SelectOption from "./component/selectOption";
// import SignInSide from './component/loginPage'

// import SearchExampleStandard from './component/SearchExampleStandard'

import ReactDOM from "react-dom";

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Map from "./component/Map";

function App() {
  return (
    <Provider store={store}>
      <>
        <div className="App">
          <Router>
            <div className="container">
              <Switch>
                <Route exact path="/" component={Login}></Route>
                <Route path="/login" component={ChooseOption}></Route>
                <Route path="/addWaypoints" component={Main}></Route>
                <Route path="/searchNearby" component={SearchNearBy}></Route>
                {/* <Route path='/logout' component={Login}></Route>   */}
              </Switch>
            </div>
          </Router>
        </div>
      </>
    </Provider>
  );
}

export default App;
