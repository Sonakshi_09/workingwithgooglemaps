import React from 'react'
import {withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer} from 'react-google-maps'

class MapContain extends React.Component{
    // constructor(props) {
    //     super(props);
    // }

    // componentDidMount() {
    //     navigator.geolocation.getCurrentPosition(
    //       function(position) {
    //         console.log("THIS IS CURRENT POSITION",position);
    //       },
    //       function(error) {
    //         console.error("Error Code = " + error.code + " - " + error.message);
    //       }
    //     );
    //   }

      render(){
          <div>
              <h2>I am routed to the nearby places page.</h2>
           
    const DirectionsComponent = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBHpzxtPZef39oeGQcWUR5U7P1Acf4UHn8&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `400px` }} />,
        containerElement: <div style={{ width: `100%` }} />,
        mapElement: <div style={{height: `600px`, width: `750px` }}  />,
        
      }),
      withScriptjs,
      withGoogleMap,
      lifecycle({
              

     

        componentDidMount() { 

  //         const store= createStore(reducer)
  //  console.log("Initial state",store.getState())
  //  const unsubscribe=store.subscribe(()=>console.log("updated state",store.getState()))
  //  store.dispatch(updatelocation())
  //  unsubscribe()
  
            // var waypts=[]
          
          // console.log("inside",self.props.latlong);
          // for(var i=0;i<=self.props.waypts.length-1;i++)
    // {
    //  waypts.push({location:self.props.waypts[i],stopover:true}); 
    // }
    // console.log("waypoints",waypts);

          const DirectionsService = new google.maps.DirectionsService();
          var first = new google.maps.LatLng(30.741482, 76.768066);
          // var second = new google.maps.LatLng(42.496401, -124.413126);
          
        //   console.log("DirectionService",DirectionService);
          DirectionsService.route({
            
              
            origin: new google.maps.LatLng(self.state.lat,self.state.lng ),
            destination: new google.maps.LatLng(self.state.latlong.lat,self.state.latlong.lng ),
            waypoints: self.state.new_waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING,
          }, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
this.setState({
                directions: {...result},
                markers: true
              })
            } else {
                console.log("yayayayaya");
              console.error(`error fetching directions ${result}`);
            }
          });
        }
      })
    )(props =>
      <GoogleMap
        defaultZoom={3}
      >
        {props.directions && <DirectionsRenderer directions={props.directions} suppressMarkers={props.markers}/>}
      </GoogleMap>
    );
        
        </div>
    return(
        <DirectionsComponent
        />
    );
    }  
}
export default MapContain;