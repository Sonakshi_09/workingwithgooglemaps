/*global google*/
import React from 'react'
import {withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer} from 'react-google-maps'
import  { compose, withProps, lifecycle } from 'recompose'

class MapContain extends React.Component{
    // constructor(props) {
    //     super(props);
    // }

    // componentDidMount() {
    //     navigator.geolocation.getCurrentPosition(
    //       function(position) {
    //         console.log("THIS IS CURRENT POSITION",position);
    //       },
    //       function(error) {
    //         console.error("Error Code = " + error.code + " - " + error.message);
    //       }
    //     );
    //   }

      render(){
           
    const DirectionsComponent = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBHpzxtPZef39oeGQcWUR5U7P1Acf4UHn8&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `400px` }} />,
        containerElement: <div style={{ width: `100%` }} />,
        mapElement: <div style={{height: `600px`, width: `750px` }}  />,
        
      }),
      withScriptjs,
      withGoogleMap,
      lifecycle({
        componentDidMount() { 
            navigator.geolocation.getCurrentPosition(
                      function(position) {
                        console.log("THIS IS CURRENT POSITION",position);
                      },
                      function(error) {
                        console.error("Error Code = " + error.code + " - " + error.message);
                      }
                    );



          const DirectionsService = new google.maps.DirectionsService();
          var first = new google.maps.LatLng(30.741482, 76.768066);
          var second = new google.maps.LatLng(42.496401, -124.413126);
          
          console.log("DirectionService",DirectionService);
          DirectionsService.route({
            
              
            origin: new google.maps.LatLng(28.69,77.89),
            destination: new google.maps.LatLng(self.state.latlong.lat,self.state.latlong.lng ),
            waypoints: self.state.new_waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING,
          }, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
this.setState({
                directions: {...result},
                markers: true
              })
            } else {
                console.log("yayayayaya");
              console.error(`error fetching directions ${result}`);
            }
          });
        }
      })
    )(props =>
      <GoogleMap
        defaultZoom={3}
      >
        {props.directions && <DirectionsRenderer directions={props.directions} suppressMarkers={props.markers}/>}
      </GoogleMap>
    );
        
    return(
        <DirectionsComponent
        />
    );
    }  
}
export default MapContain;