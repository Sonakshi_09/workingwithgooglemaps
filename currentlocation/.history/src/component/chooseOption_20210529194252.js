import React from "react";

class ChooseOption extends React.Component {
  render() {
    return (
        <div class="ui placeholder segment">
        <div class="ui two column very relaxed stackable grid">
          <div class="column wayPoints">
           
          </div>
          <div class="middle aligned column">
            <div class="ui big button">
              <i class="signup icon"></i>
              Sign Up
            </div>
          </div>
        </div>
        <div class="ui vertical divider">
          Or
        </div>
      </div>
    );
  }
}

export default ChooseOption;
