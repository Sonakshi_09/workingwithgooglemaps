import React from "react";
import  './chooseOption.css'
class ChooseOption extends React.Component {
  render() {
    return (
      <div class="ui center aligned basic segment">
        <div class="ui left ">
          <div class="wayPoints"></div>
        </div>
        <div class="ui horizontal divider">Or</div>
        <div class="ui teal labeled icon button">
          Create New Order
          <i class="add icon"></i>
        </div>
      </div>
    );
  }
}

export default ChooseOption;
