import React from "react";
import "./chooseOption.css";
class ChooseOption extends React.Component {
  render() {
    return (
      <div class="ui equal width center aligned padded grid">
        <div class="row wayRemove"></div>
        <div class="white column right-design">HEALLOOO</div>
        <div class="ui vertical divider verticalDivide" />
        <div className="way-design">
          <div class="ui center aligned basic segment wayRemove">
            <div class="ui left wayPoints">
            <button class=" inverted black button change">Red</button>
            </div>
            <div class="ui horizontal divider divideOR">OR</div>
            <div class="ui teal nearbyPlaces">
            <button class="ui inverted red button">Red</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseOption;
