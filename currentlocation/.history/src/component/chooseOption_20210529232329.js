import React from "react";
import { Button } from "semantic-ui-react";
import "./chooseOption.css";
class ChooseOption extends React.Component {
  render() {
    return (
      <div class="ui equal width center aligned padded grid">
        <div class="row wayRemove"></div>
        <div class="column">
            <div class="row first"> Heya</div>
            <div class="row second"> Heya</div>
        </div>
        <div class="ui vertical divider verticalDivide" />
        <div className="way-design">
          <div class="ui center aligned basic segment wayRemove">
            <div class="ui left wayPoints">
            <img className="wayPoints" src={} alt='AddWaypoints'></img>
            <Button className="ui inverted change" > Add Waypoints</Button>
            </div>
            <div class="ui horizontal divider divideOR">OR</div>
            <div class="ui teal nearbyPlaces">
            <Button className="ui inverted change"> Search Nearby Places</Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseOption;
