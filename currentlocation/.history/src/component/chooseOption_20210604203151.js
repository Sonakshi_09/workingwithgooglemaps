import React from "react";
import pic1 from "../pic1.png"
import pic2 from "../pic2.png"
import { Button } from "semantic-ui-react";
import "./chooseOption.css";
import { Link } from 'react-router-dom';

class ChooseOption extends React.Component {
  render() {
    return (
      <div class="ui equal width center aligned padded grid">
        <div class="row wayRemove"></div>
        <div class="column">
            <div class="row first "> <br/>Want to explore different places?<br/><br/>You are at the right place!<br/><br/>Tap Add WayPoints and EXPLORE!!</div>
            <div class="row second"><br/>Want to explore different places?<br/><br/>You are at the right place!<br/><br/>Tap Search Nearby and EXPLORE!!</div>
        </div>
        <div class="ui vertical divider verticalDivide" />
        <div className="way-design">
          <div class="ui center aligned basic segment wayRemove">
            <div class="container">
            <img className="wayPoints" src={pic1} alt='AddWaypoints' width="850px"></img>
            <Link to='/addWaypoints'>
            <Button className="ui inverted change" > Add Waypoints</Button>
            </Link>
            </div>
            <div class="ui horizontal divider divideOR">OR</div>
            <div class="ui">
            <img className="nearbyPlaces" src={pic2} alt='Search NearBy Places ' width="850px"></img>
            <Link to='/addWaypoints'>
            <Button className="ui inverted change"> Search Nearby Places</Button>
            </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseOption;
