import React from "react";
import pic1 from "../pic1.png";
import pic2 from "../pic2.png";
import { Button } from "semantic-ui-react";
import "./chooseOption.css";
import { Link } from "react-router-dom";

class ChooseOption extends React.Component {
  render() {
    return (
      <div class="card card0">
        <div class="ui equal width center aligned padded grid">
          <div class="row remove">
            <div className="left-design"></div>
            <div class="white column right-design">
              <div className="dynamicText" id="changeText"></div>
              <div className="subhead-text">Where there's &nbsp;<img src={textLogo} alt="there is a way!!" width="40" height="40"/>&nbsp;&nbsp;there is a way!!</div>
              <GoogleLogin
                className="btnGoogle"
                clientId={clientId}
                onSuccess={this.onSuccess}
                onFailure={this.onFailure}
                cookiePolicy={"single_host_origin"}
                isSignedIn={true}
                buttonText="Sign In With Google"
              />
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseOption;

//------------------------------
{
  /* <div class="ui equal width center aligned padded grid main">
<div class="row wayRemove"></div> */
}
{
  /* <div class="ui vertical divider verticalDivide" /> */
}
{
  /* <div className="way-design">
  <div class="ui center aligned basic segment wayRemove">
    <div class="container"> */
}
{
  /* <img
        className="wayPoints"
        src={pic1}
        alt="AddWaypoints"
        width="850px"
      ></img> */
}
//   <Link to="/addWaypoints">
//     <Button className="ui inverted change"> Add Waypoints</Button>
//   </Link>
// </div>
{
  /* <div class="ui horizontal divider divideOR">OR</div> */
}
// <div class="ui">
{
  /* <img
        className="nearbyPlaces"
        src={pic2}
        alt="Search NearBy Places "
        width="850px"
      ></img> */
}
//       <Link to="/searchNearby">
//         <Button className="ui inverted change">
//           {" "}
//           Search Nearby Places
//         </Button>
//       </Link>
//     </div>
//   </div>
// </div>
// </div>
