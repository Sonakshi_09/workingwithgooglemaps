import React from "react";
import pic1 from "../pic1.png";
import pic2 from "../pic2.png";
import { Button } from "semantic-ui-react";
import "./chooseOption.css";
import { Link } from "react-router-dom";

class ChooseOption extends React.Component {
  render() {
    return (
      <div>
        <div class="ui placeholder segment left-waypoint">
          <div class="ui two column very relaxed stackable grid">
            <div class="column">
              <div>
                <img
                  className="wayPoints"
                  src={pic1}
                  alt="AddWaypoints"
                  width="50px"
                ></img>
              </div>
              <div class="ui form">
                <Link to="/addWaypoints">
                  <Button className="ui inverted change"> Add Waypoints</Button>
                </Link>
              </div>
            </div>
            <div class="middle aligned column">
              <Link to="/searchNearby">
                <Button className="ui inverted change">
                  Search Nearby Places
                </Button>
              </Link>
            </div>
          </div>
          <div class="ui vertical divider">OR</div>
        </div>
      </div>
    );
  }
}

export default ChooseOption;

//------------------------------
{
  /* <div class="ui equal width center aligned padded grid main">
<div class="row wayRemove"></div> */
}
{
  /* <div class="ui vertical divider verticalDivide" /> */
}
{
  /* <div className="way-design">
  <div class="ui center aligned basic segment wayRemove">
    <div class="container"> */
}
{
  /* <img
        className="wayPoints"
        src={pic1}
        alt="AddWaypoints"
        width="850px"
      ></img> */
}
//   <Link to="/addWaypoints">
//     <Button className="ui inverted change"> Add Waypoints</Button>
//   </Link>
// </div>
{
  /* <div class="ui horizontal divider divideOR">OR</div> */
}
// <div class="ui">
{
  /* <img
        className="nearbyPlaces"
        src={pic2}
        alt="Search NearBy Places "
        width="850px"
      ></img> */
}
//       <Link to="/searchNearby">
//         <Button className="ui inverted change">
//           {" "}
//           Search Nearby Places
//         </Button>
//       </Link>
//     </div>
//   </div>
// </div>
// </div>
