import React from "react";
import maps1 from "../maps1.jpg";
import { routewaypt, sendid } from "../redux";
import { routedes } from "../redux";
import { token } from "../redux";
//import ReactDOM from 'react-dom';
import { GoogleLogin } from "react-google-login";
import FacebookLogin from "react-facebook-login";
import "./login.css";
import axios from "axios";

import { connect } from "react-redux";
// import Logout from './component/logout';

const clientId =
  "111458893267-asfnr0bb67s3nifuhvl9e3drm0tssj1o.apps.googleusercontent.com";
  var text = [ "Don't Worry, We Got You!!" ,"Are you Lost?","SignIn and Find Yourself! "];
  var counter = 0;
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: "",
      des_arr: [],
      waypt_arr: [],
      final_arr: [],
    };
    this.onSuccess = this.onSuccess.bind(this);
    this.onFailure = this.onFailure.bind(this);
  }

  onSuccess = (res) => {
    //    var self=this;
    console.log("[Login Success] currentUser:", res.profileObj);
    alert("Logged in");
    // this.setState({
    //     sid: '2'
    // },()=>{
    //     console.log("hjh,adjs",this.state.sid);
    // })

    // d);console.log("hardcoded id",this.state.si
    console.log("object", res.profileObj);
    axios
      .post("http://localhost:9000/login", {
        email: res.profileObj.email,
        name: res.profileObj.name,
      })
      .then(
        (response) => {
          console.log(response);
          console.log(response.data.id);
          console.log("token", response.data.token);
          this.props.set_token(response.data.token);
          // if(response.data.destination){
          //     for(var i=0;i<response.data.destinations.length;i++){
          //         for(var j=0;j<response.data.destinations.length;j=j+2){
          //             if(response.data.destinations[i][j]){
          //             console.log(response.data.destinations[i][j].address)
          //             this.state.des_arr.push(response.data.destinations[i][j].address);

          //         }
          //     }
          //     }
          // }
          //    if(response.data.destination){
          //     for(var i=0;i<response.data.destinations.length;i++){
          //         for(var j=1;j<response.data.destinations.length;j=j+1){

          //             if(response.data.destinations[i][j]){
          //             for(var k=0;k<response.data.destinations[i][j].length;k=k+1){
          //                   console.log(response.data.destinations[i][j][k])
          //                   this.state.waypt_arr.push(response.data.destinations[i][j][k].address)
          //             }
          //             this.state.final_arr.push(this.state.waypt_arr);
          //             this.setState({
          //                 waypt_arr:[]
          //             })
          //         }
          //         }
          //     }
          //     }
          //     console.log("final array",this.state.final_arr);

          //     this.props.set_waypt_route(this.state.final_arr);
          //     console.log(this.props.set_waypt_route);
          //     console.log("des+arr",this.state.des_arr);
          //     this.props.set_des_route(this.state.des_arr);
          //     console.log(this.props.set_des_route);
          //     console.log("waypt_arr",this.state.waypt_arr);
          this.setState({
            sid: response.data.id,
          });
          console.log("done");
          console.log(this.state.sid);
          this.props.history.push("/login");
          this.props.set_id(this.state.sid);
        },
        (error) => {
          console.log(error);
          console.log("hello");
        }
      );
  };
  onFailure = (res) => {
    console.log("[Login failed] res:", res);
  };

  change = () => {
    console.log("Hello");
    var elem = document.getElementById("changeText");
    elem.innerHTML = text[counter];
    counter++;
    if (counter >= text.length) {
      counter = 0;
      // clearInterval(inst); // uncomment this if you want to stop refreshing after one cycle
    }
  };
  inst = setInterval(this.change, 3000);
  render() {
    return (
      <div>
        <div class="ui equal width center aligned padded grid">
          <div class="row remove">
            <div className="left-design"></div>
            <div class="white column right-design">
              <div className="dynamicText" id="changeText"></div>
              <div>Where there's <img src={maps1} alt="there is a way!!" width="20" height="20"/>there is a way!!</div>
              <GoogleLogin
                className="btnGoogle"
                clientId={clientId}
                onSuccess={this.onSuccess}
                onFailure={this.onFailure}
                cookiePolicy={"single_host_origin"}
                isSignedIn={true}
                buttonText="Sign In with Google"
              />
              <br />
            </div>
          </div>
          <div class="ui three column grid bottom-design">
            <div class="column imageCreater"></div>
            <div class="column">
              <div class="ui segment">SECOND</div>
            </div>
            <div class="column">THIRD</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    set_id: (value) => {
      dispatch(sendid(value));
    },
    set_token: (value) => {
      dispatch(token(value));
    },

    //    set_des_route:(value)=>{
    //         dispatch(routedes(value))

    //    },
    //    set_waypt_route:(value)=>{
    //        dispatch(routewaypt(value))
    //    }
  };
};

export default connect(null, mapDispatchToProps)(Login);
