import React, { Component } from 'react'
import MapContain from './Map2'
import NearbySearch from "./nearbysearch"
// import { Checkbox } from 'semantic-ui-react'
// import {setplace} from '../redux'
import {connect} from 'react-redux'
import axios from 'axios'
import { Grid, Button } from 'semantic-ui-react'

class SearchNearBy extends React.Component{
    constructor(props) {
        super(props);
        this.props=props;
        this.state={
            datasave:[],
            datafetch:[]   
        };
        this.fetchplace=this.fetchplace.bind(this)
    }
    saveplace(){
    console.log("INSIDE SAVEROUTE PROPS",this.props);
    console.log("TOKEN WALE KE ANDER HUN BHAI",this.props.token);
    var data= this.props.p_value;
    console.log("DATA KE ANDER KYA HAI",data);
    axios.post('http://localhost:9000/locations/saveplaces', 
    data,{headers:{'token':this.props.token}}).then((response)=>{
            console.log(response);
            alert("Places saved successfully");
    
        },(error)=>{
            console.log(error);
            console.log("OOPS ITS THE ERROR PHASE.");
        
        });
    }

   async fetchplace(){
        await this.setState({
            datasave:[]
        });
        console.log("ID KI VALUE",this.props.user);
        var id=this.props.user;
        console.log("TOKEEEEN",this.props.token);
        
        axios.post('http://localhost:9000/locations/getplaces',
        {headers:{'token': this.props.token}})
        .then(
            (response)=>{
                //console.log("TOKEEEEN",this.props.token)
        console.log(response); 
        console.log("ME AAGYAAA");

        for(var i=0;i<response.data.length;i++)
        this.state.datasave.push(response.data[i]);

        this.setState({
            datafetch:this.state.datasave
        });
        console.log("DATA FETCH KI VALUE",this.state);
        console.log("SETSTATE KE ANDER KA DATA", this.state.datasave);
        console.log("DATA FETCH KI VALUE",this.state.datafetch)
    });
    
    }
    render(){
        return(
            <div>
            <div>
            <h2>Looking for nearby places.  You are at Right Place </h2>
            <NearbySearch> 
            </NearbySearch>
            </div>
            <div class="ui grid row">
            <Grid>
                <Grid.Row>
                <Grid.Column width={12}>
                    <MapContain>    
                        </MapContain>
                </Grid.Column>
                <Grid.Column width={4}>
                <div>
            <h2>Nearby Places Searched Are:</h2>
            <ul id='places'>
            </ul>
                 </div>&nbsp; &nbsp;&nbsp;&nbsp;
                 <div >
                     <ul >
                     <Button secondary type='submit' onClick={() => this.saveplace()}>SAVE SELECTED PLACES</Button>
                     </ul>
                    <ul>
                    &nbsp;&nbsp; <Button secondary type='submit' onClick={() => this.fetchplace()}>FETCH SAVED PLACES</Button>
                </ul>
                 </div>
                 <div>
                   <ul>
                    {this.state.datasave.map((value)=>{
                        console.log("INSIDE MAP FUNCTION VALUE",value);
                  return(
                  <table>
                    <td>
                    {value}
                    </td>
                  </table>
                )}
                )
                  }
                  </ul>
                  </div>
            </Grid.Column>
            </Grid.Row>
        </Grid>
        </div>
        </div>
        );
    }
}

const dataStateToProps = state => {
    return {
      // latlong : state.values,
      // waypts: state.point,
      // value:state.check_status,
      // des_latlong:state.saved_des,
      // waypts_array:state.saved_waypts

      p_value:state.place_val,
      token:state.token,
      user:state.id
      
    }
  
  }
export default connect(dataStateToProps,null)(main2); 