/*global google*/ 
import React from 'react'
import { Segment, Input, Icon, Dropdown } from 'semantic-ui-react'
import _ from 'lodash'
import {connect} from 'react-redux'
import  { compose, withProps, lifecycle } from 'recompose'
// import PropTypes from 'prop-types';
//import { checkPropTypes } from 'prop-types';
import { MapContain } from "./Map2";
import {setpos,setplace} from '../redux'
import props from 'prop-types';
import $ from 'jquery';

var map;
var service;
var infowindow;
// const google = window.google;
class NearbySearch extends React.Component{
    constructor(props) {
        super(props);
        this.props=props;

        this.state={
            id:'',
            name:'',
            type:[],
            arr:[],
        };
        //console.log("PROPS IN CONSTRUCTOR",this.props);
        // this.state = {
        //     rad:''
        // };
        // console.log(props);
      }
    //   componentDidMount() {
    //     //   console.log("PROPS",this.props);
    //   }

      initialize() {
        // const self=this;
        console.log("props",this.props.value);
        var radius_value=document.getElementById("rad").value;
        const text_value=document.getElementById("val").value;
        
        console.log("LAT props",this.props.value.lat); 
        console.log("LONG props",this.props.value.lng);
        
        var pyrmont = new google.maps.LatLng({lat: this.props.value.lat, lng: this.props.value.lng});
        const map = new google.maps.Map(document.getElementById('map'), {
            center: pyrmont,
            zoom: 15
          });
        console.log("VALUE INSIDE MAP",map);
        var request = {
          location: pyrmont,
          radius: radius_value,
          type: [text_value]
        };
        
        service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request,(results, status, pagination) => {
                if (status !== "OK") 
                    return;
              
        this.createMarkers(results, map);
        this.createcircle(radius_value, map);

        // for (let i = 0,res; (res = results[i]); i++) {
        //     console.log("RESULT", res)
        //     console.log("INSIDE RESULTS VALUES OF ID",res.place_id);
        //     console.log("INSIDE RESULTS VALUES OF NAME",res.name);
        //     console.log("TYPE OF PLACE",res.types);
        //     console.log("LA LONG OF PLACES",res.geometry.location.lat);
        //     self.setState({
        //         id:res.place_id,
        //         name:res.name,
        //         type:res.types,
        //     })
        //     // console.log("VALUE INSIDE SELF.STATE",self.setState);
        //     // console.log("RESULT", results)
        //     var data_obj={
        //         id:self.state.id,
        //         name:self.state.name,
        //         type:self.state.type,
        //     }
        //     console.log("VALUE IN THE DATA OBJECT IS SEEN HERE",data_obj);
        //     self.props.set_place(data_obj);
        //     console.log("SET PLACE VALUE",self.props.set_place);
        // }
            }
          );
      }

    createMarkers(places, map) {
        const self=this;
        alert("HELLO I AM IN CREATE MARKERS")
        const bounds = new google.maps.LatLngBounds();
        const placesList = document.getElementById("places");
        for (let i = 0, place; (place = places[i]); i++) {
          const image = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25),
          };
        const infowindow = new google.maps.InfoWindow();
          const marker=new google.maps.Marker({
            map,
            icon: image,
            title: place.name,
            position: place.geometry.location,
          });
          marker.addListener("click", () => {
            infowindow.setContent(place.name);
            infowindow.open(map, marker);
          });
    
          var li = document.createElement('input');
          var label_box= document.createElement('label');
          var breaakkk= document.createElement('br');
          var brrr=document.createElement('br');
          li.type = 'checkbox';
          li.id = 'placelist_' + i;
          li.name = place.name;
          label_box.htmlFor=li.id;
          label_box.value=li.label;
          label_box.appendChild(document.createTextNode(place.name));
    
          placesList.appendChild(li);
          placesList.appendChild(label_box);
          placesList.appendChild(breaakkk);
          bounds.extend(place.geometry.location);
        }
        
        $(document).on('click', '[type=checkbox]', function(e) {
            console.log("HELLOO I AM CLICKED");
            console.log("EVENT",e.target);
            console.log("EVENT NAMES",e.target.name);
            var push_arr=[];
            if(e.target.checked == true){
                push_arr.push(e.target.name);
                
                console.log("push array values",push_arr);
            }
            else{
                for(var i=0;i<self.state.arr.length;i++)
                {
                    if(self.state.arr[i]===e.target.name){
                        self.state.arr.splice(i,1);
                    }
                }
            }     
            self.setState({
                arr: [...self.state.arr,push_arr]
            });
            var data_array= self.state.arr;
            console.log("ARAAY KI VALUE",data_array);
            self.props.set_place(data_array);

          });
        
        map.fitBounds(bounds);
      }
      createcircle(rad, map){
        // console.log("TYPE OF PROPS",typeof(props.value.lat));
        // console.log("VALUE OF PROPS> LAT",props.value.lat);
        var obj={
            lat:this.props.value.lat,
            lng:this.props.value.lng
        }
        console.log("VALUES OF OBJECT",obj);
        const radius_val=new google.maps.Circle({
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#FF0000",
          fillOpacity: 0.35,
          map,
          center: obj,
          radius: parseFloat(rad),
          editable: true
        });
        console.log("VALUE OF CIRCLE", radius_val)
        console.log("VALUE OF RADIUS IN ARRAY",radius_val.radius);
    }


 
render(){ 
        return(
                <Segment inverted>
                  <Input id='val' inverted placeholder='Search nearby places...' />
                  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
                  <Input id='rad' type='number' inverted placeholder='Set Radius...'  />
                  &nbsp; &nbsp;&nbsp; &nbsp;
                  <Icon name='chevron circle right' onClick={() => this.initialize()}/>
                  &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                  {/* <Icon name='chevron circle right' onClick={() => this.saveroute()}/> */}
                </Segment>
                            
        );
    }
}


const dataDispatchToProps = (dispatch) => {
    return {
       set_place:(p_value)=>{
           console.log("WHAT IS YOUR VALUE",p_value)
       dispatch(setplace(p_value))
       }   
    }
  }

const mapStateToProps = state => {
      return {
        // latlong : state.values,
        // waypts: state.point,
        // value:state.check_status,
        // des_latlong:state.saved_des,
        // waypts_array:state.saved_waypts
        value:state.pos
        
      }
    
    }


export default connect(mapStateToProps,dataDispatchToProps)(NearbySearch)

//PRACTICE CODE
  
    //   getresults(results, status) {
    //       console.log("results",results)
    //     if (status == google.maps.places.PlacesServiceStatus.OK) {
    //       for (var i = 0; i < results.length; i++) {
    //         this.createMarker(results[i]);
            
    //       }
    //     }
    //   }
    //   createMarkers(place) {
    //       const marker = new google.maps.Marker({
    //         map,
    //         position: place.geometry.location,
    //       });
    //       google.maps.event.addListener(marker, "click", () => {
    //         infowindow.setContent(place.name);
    //         infowindow.open(map);
    //       });
          
    //     }
