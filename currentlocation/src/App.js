import React, { Component } from "react";
import "./App.css";
import ImageExampleImage from "./component/ImageExampleImage";
import Login from "./component/Login";
import Logout from "./component/logout";
// import { useHistory } from "react-router-dom";
import Main from "./component/Main";
import SearchNearBy from "./component/SearchNearBy";
import ChooseOption from "./component/chooseOption";
// import SignInSide from './component/loginPage'
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Map from "./component/Map";

function App() {
  return (
    <Provider store={store}>
      <>
        <div className="App">
          <Router>
            <div className="container">
              <Switch>
                <Route exact path="/" component={Login}></Route>
                <Route path="/login" component={ChooseOption}></Route>
                <Route path="/addWaypoints" component={Main}></Route>
                <Route path="/searchNearby" component={SearchNearBy}></Route>
                <Route path='/' component={Logout}></Route>  
              </Switch>
            </div>
          </Router>
        </div>
      </>
    </Provider>
  );
}

export default App;
