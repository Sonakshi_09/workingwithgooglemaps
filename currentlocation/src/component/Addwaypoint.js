import React from "react";

import {
  checkstatus,
  savedroutesdes,
  savedrouteswaypts,
  updatelocation,
} from "../redux";
import { addwaypoint } from "../redux";
import "./main.css";
// import {currloc} from '../redux'
import { sendid } from "../redux";
// import {connect} from 'react-redux'

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
// import { useContext } from 'react';
// import { AppContext } from './Main'

import { createStore } from "redux";
import { connect } from "react-redux";
import { Icon, Input, Button, Grid, Segment, Label, Popup } from "semantic-ui-react";
import axios from "axios";
import { blue, blueGrey, green } from "@material-ui/core/colors";

// const baseURL = process.env.BASE_URL;

// const changeInputValue = (newValue) => {

//   dispatch({ type: 'UPDATE_LOCATION', data: newValue,});
// };

class SearchLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      latlong: "",
      waypt: [],
      list: [],
      arr: [],
      pusharr: [],
      user_id: "",
      final_waypt: [],
      final_arr: [],
      last_arr: [],
      lat_arr: [],
      long_arr: [],
      waypt_lat: [],
      waypt_long: [],
      final_waypt_lat: [],
      final_waypt_long: [],
      lat_long_waypts: [],
      waypts_push: [],
      obj: {},
      checking: false,
    };
    this.submit = this.submit.bind(this);
    this.setblank = this.setblank.bind(this);
    this.saveroute = this.saveroute.bind(this);
    this.delete = this.delete.bind(this);
    this.clear = this.clear.bind(this);
    this.routes = this.routes.bind(this);
    this.rendermap = this.rendermap.bind(this);
  }

  async rendermap(index) {
    await this.setState({
      checking: true,
    });

    await this.setState({
      obj: {},
      waypts_push: [],
    });
    this.props.set_checking(this.state.checking);
    console.log(this.props.set_checking);
    console.log(this.state.last_arr[index]);
    console.log(this.state.final_waypt_lat[index]);
    console.log(this.state.final_waypt_long[index]);
    console.log(this.state.lat_arr[index]);
    console.log(this.state.long_arr[index]);
    var pointsobj = {
      lat: this.state.lat_arr[index],
      lng: this.state.long_arr[index],
    };
    console.log("new object", pointsobj);
    this.props.set_des(pointsobj);

    var wayptsobj = {
      lat: this.state.final_waypt_lat[index],
      lng: this.state.final_waypt_long[index],
    };
    console.log("waypoint array sending", wayptsobj);
    // this.props.set_waypts(wayptsobj);

    for (var i = 0; i < wayptsobj.lat.length; i++) {
      //  this.state.lat_long_waypts.push(wayptsobj.lat[i])
      //  this.state.lat_long_waypts.push(wayptsobj.lng[i])
      this.state.obj = {
        lat: wayptsobj.lat[i],
        lng: wayptsobj.lng[i],
      };
      this.state.waypts_push.push(this.state.obj);
      console.log("push lat long", this.state.obj);
      await this.setState({
        obj: {},
      });
      // console.log(this.state.waypts_array.lat[i])
    }
    console.log("obj", this.state.waypts_push);
    this.props.set_waypts(this.state.waypts_push);
  }

  async routes() {
    this.setState({
      last_arr: [],
      arr: [],
      pusharr: [],
      final_waypt: [],
      final_arr: [],
    });
    console.log(this.state.user_id);
    await this.setState({
      user_id: this.props.id,
    });
    console.log(this.state.user_id);
    console.log(this.props.id);
    console.log("token", this.props.token);
    axios
      .post(
        "http://localhost:9000/locations",
        { id: this.state.user_id },
        { headers: { token: this.props.token } }
      )
      .then((response) => {
        console.log(response);

        // console.log(response.data.destinations[5][0])

        // console.log(response.data.destinations[0][0])
        for (var i = 0, j = 0; i < response.data.destinations.length; i++) {
          console.log(response.data.destinations[i][j].address);
          this.state.arr.push(response.data.destinations[i][j].address);

          this.state.lat_arr.push(response.data.destinations[i][j].lat);
          console.log("lata-arr", this.state.lat_arr);

          this.state.long_arr.push(response.data.destinations[i][j].lng);
          console.log("long_arr", this.state.long_arr);
        }

        console.log("destinations", this.state.arr);

        for (var i = 0, j = 1; i < response.data.destinations.length; i++) {
          for (var k = 0; k < response.data.destinations[i][j].length; k++) {
            this.state.pusharr.push(
              response.data.destinations[i][j][k].address
            );
            this.state.waypt_lat.push(response.data.destinations[i][j][k].lat);
            this.state.waypt_long.push(response.data.destinations[i][j][k].lng);
          }
          this.state.final_waypt.push(this.state.pusharr);
          this.state.final_waypt_lat.push(this.state.waypt_lat);
          this.state.final_waypt_long.push(this.state.waypt_long);
          this.setState({
            pusharr: [],
            waypt_lat: [],
            waypt_long: [],
          });
        }

        console.log("waypoints", this.state.final_waypt);
        console.log("final_waypt_lat", this.state.final_waypt_lat);
        console.log("final_waypt_long", this.state.final_waypt_long);

        for (var i = 0; i < response.data.destinations.length; i++) {
          for (var k = 0; k < this.state.final_waypt[i].length; k++) {
            this.state.final_arr.push(this.state.final_waypt[i][k]);
          }
          this.state.final_arr.push(this.state.arr[i]);
          console.log("final hdh final", this.state.final_arr);
          this.state.last_arr.push(this.state.final_arr);
          this.setState({
            final_arr: [],
          });
        }
        console.log("last array", this.state.last_arr);
      });
    // console.log("props _des",this.props.des_route);
    // console.log("waypt_arr",this.props.waypt_arr);

    //  for(var i=0;i<this.props.des_route.length;i++){
    //    console.log("hello")

    //    for(var j=0;j<this.props.waypt_arr.length;j++){
    //      this.state.arr.push(this.props.waypt_arr[i][j]);

    //    }

    //   this.state.arr.push(this.props.des_route[i]);
    //   this.state.pusharr.push(this.state.arr);
    //   await this.setState({
    //      arr:[]
    //    })

    //      console.log("pusharr",this.state.pusharr)

    //  }
    //   console.log("final array",this.state.pusharr)
  }

  handleChange = (address) => {
    this.setState({ address });
  };
  async clear(index) {
    console.log("existing Array", this.state.list);
    const list = Object.assign([], this.state.list);
    list.splice(index, 1);
    console.log("list", list);

    await this.setState((state, props) => ({
      list: list,
    }));
    console.log("deleted array", this.state.list);
    console.log("existing waypoints", this.state.waypt);
    const waypt = Object.assign([], this.state.waypt);
    waypt.splice(index, 1);
    console.log("now waypt", waypt);
    await this.setState({
      waypt: waypt,
    });
    console.log("deleted waypoints", this.state.waypt);
  }
  saveroute() {
    console.log("destination", this.props.destination);
    console.log("entire waypoints", this.props.waypoints);
    console.log("sending id", this.props.id);
    console.log("adress list", this.state.list);
    console.log("destination address", this.props.des_add);
    // console.log("current location send",this.props.curr_loc);
    // console.log("base url", baseURL);
    var data = {
      destination: this.props.destination,
      waypoints: this.props.waypoints,
      id: this.props.id,
      des_add: this.props.des_add,
      waypt_address: this.state.list,
    };
    axios
      .post("http://localhost:9000/locations/location", data, {
        headers: { token: this.props.token },
      })
      .then(
        (response) => {
          console.log(response);
          alert("Route saved successfully");
        },
        (error) => {
          console.log(error);
          console.log("hello");
        }
      );
  }

  delete(key) {
    // var hello=this.state.list;
    // hello.delete(key);
    // console.log("this",this.state.hello)
  }

  setblank() {
    this.setState({
      list: [],
    });
    console.log(this.state.waypt);
  }
  submit() {
    this.props.set_waypoint(this.state.waypt);
  }
  handleSelect = async (address) => {
    await this.setState({
      checking: false,
    });
    this.props.set_checking(this.state.checking);
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then(
        async (latLng) => {
          console.log("Success", latLng);
          await this.setState({
            latlong: latLng,
          });

          this.state.waypt.push(this.state.latlong);
          console.log("new values", this.state.waypt);
          this.state.list.push(address);
          console.log(" list", this.state.list);

          console.log("set_waypoint", this.props.set_waypoint);

          // onChange={e => changeInputValue(e.target.value)}
          console.log("state", this.state.latlong);
          const value = this.state.latlong;
          // console.log("value",value)

          // console.log(set_value);
        }
        // console.log("latlong",latLng)
      )

      .catch((error) => console.error("Error", error));
  };

  render() {
    // const points = this.state.list;

    console.log("destination here", this.props.destination);

    const like = this.state.latlong;
    // const {state, dispatch} = useContext(AppContext);

    //  console.log("Initial state",store.getState())
    //  const unsubscribe=store.subscribe(()=>console.log("updated state",store.getState()))
    //  store.dispatch(updatelocation())
    //  unsubscribe()

    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        // onChange={e => changeInputValue(e.target.value)}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <Grid>
              {/* <Grid.row> */}
              <Grid.Column width={16}>
                {/* <Grid.row> */}
                <Segment raised color="black">
                  <Label color="red" ribbon>
                    WAYPOINTS
                  </Label>
                  <Input
                    // <input
                    {...getInputProps({
                      placeholder: "Add Waypoints...",
                      icon: "map marker alternate",
                      size: "small",
                      iconPosition: "left",
                      className: "location-search-input",
                      type: "text",
                      name: "search",
                      // ref={(el) => (this.text = el)}
                    })}
                  />

                  {/* <Icon name='users'></Icon> */}
                  <div className="autocomplete-dropdown-container">
                    {loading && <div>Loading...</div>}
                    {suggestions.map((suggestion) => {
                      const className = suggestion.active
                        ? "suggestion-item--active"
                        : "suggestion-item";
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: "#fafafa", cursor: "pointer" }
                        : { backgroundColor: "#ffffff", cursor: "pointer" };
                      return (
                        <div
                          {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                          })}
                        >
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                  <br />
                  {/* <button onClick={this.clear}>+</button> */}
                  <div>
                    <ul>
                      {this.state.list.map((value, index) => {
                        console.log(value);
                        return (
                          <table>
                            <tr>
                              <td>
                                <Label>
                                  {" "}
                                  {value}
                                  <Icon
                                    onClick={this.clear.bind(this, index)}
                                    name="delete"
                                  />
                                </Label>

                                {/* {index} */}
                              </td>
                              <td>
                                {/* <button onClick={this.clear.bind(this,index)}>x</button> */}
                                {/* this.clear(index) */}
                              </td>
                            </tr>
                          </table>
                        );
                      })}
                    </ul>
                  </div>

                  <Button compact color="blue" onClick={this.submit}>
                    Submit
                  </Button>
                  <Button compact color="blue" onClick={this.saveroute}>
                    Save Route
                  </Button>
                  {/* <button onClick={this.saveroute}>Save route</button> */}
                </Segment>
              </Grid.Column>
              {/* <button onClick={this.submit}>Submit</button> */}
              {/* <button onClick={this.setblank}>Add new Route</button> */}
              <Grid.Row>
                <Grid.Column width={16}>
                  <Segment raised color="black">
                    <div>
                      <Button
                        className="savedroutes"
                        color="facebook"
                        onClick={this.routes}
                      >
                        Previously saved routes
                      </Button>
                      {/* <button onClick={this.routes}>Previously saved routes</button> */}
                    </div>

                    <ul>
                      {this.state.last_arr.map((value, index) => {
                        return (
                          <table>
                            <td>
                              <Button
                                fluid
                                color="teal"
                                onClick={this.rendermap.bind(this, index)}
                              >
                                {value}
                              </Button>
                              {/* <button onClick={this.rendermap.bind(this,index)}>{value}</button> */}
                            </td>
                          </table>
                        );
                      })}
                    </ul>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            {/* </Grid.row> */}
            {/* </Grid.row> */}
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

//  console.log(this.state.latLng);
const mapDispatchToProps = (dispatch) => {
  //  console.log(set_value(value));

  return {
    //  set_value:(value)=>{
    // //  console.log("aaja bhyii",value);
    //  dispatch(updatelocation(value))
    //  },
    set_waypoint: (points) => {
      dispatch(addwaypoint(points));
    },
    set_checking: (value) => {
      dispatch(checkstatus(value));
    },
    set_des: (value) => {
      dispatch(savedroutesdes(value));
    },
    set_waypts: (points) => {
      dispatch(savedrouteswaypts(points));
    },

    // updatelocation : () => dispatch(this.state.latLng)
  };
};

const mapStateToProps = (state) => {
  return {
    destination: state.values,
    waypoints: state.point,
    id: state.id,
    // curr:state.curr_loc
    des_add: state.des_add,
    des_route: state.des_arr,
    waypt_arr: state.waypt_arr,
    token: state.token,
  };
};

// console.log(this.state.latlong);

export default connect(mapStateToProps, mapDispatchToProps)(SearchLocation);
