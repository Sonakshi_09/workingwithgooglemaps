import { Icon } from '@iconify/react'
import locationIcon from '@iconify/icons-mdi/map-marker'
import React from 'react'

const LocationPin = ({ text }) => (
    <div className="pin">
      <Icon icon={locationIcon} className="pin-icon" />
      <p className="pin-text">{text}</p>
    </div>
  )

  export default LocationPin