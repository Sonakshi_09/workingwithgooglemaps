/*global google*/
import React from "react";
import redux from "redux";
import { createStore } from "redux";
import latlong from "./SearchLocation";
import { checkstatus, updatelocation, setpos } from "../redux";
import props from "prop-types";
// import { useContext } from 'react';
// import { AppContext } from './Main'
import { compose, withProps, lifecycle } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker,
} from "react-google-maps";
import SearchLocation from "./SearchLocation";
import { connect } from "react-redux";

class MapContain extends React.Component {
  constructor(props) {
    super(props);

    this.state = { lat: 0, lng: 0 };
    // this.delta.bind(this);
  }
  componentDidMount() {
    const self = this;

    navigator.geolocation.getCurrentPosition(
      function (position) {
        console.log("THIS IS CURRENT POSITION", position);
        console.log("Latitude is :", position.coords.latitude);
        console.log("Longitude is :", position.coords.longitude);

        self.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });

        var obj = {
          lat: self.state.lat,
          lng: self.state.lng,
        };
        //   console.log("OBJECT VALUE",obj);
        self.props.set_pos(obj);

        // console.log("LAT AND LONG",self.state.lat,self.state.lng);
      },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      }
    );
  }
  render() {
    const self = this;

    const GoogleMapExample = withGoogleMap((props) => (
      <GoogleMap
        defaultCenter={{ lat: self.state.lat, lng: self.state.lng }}
        defaultZoom={13}
      >
        <Marker
          position={{ lat: self.state.lat, lng: self.state.lng }}
        ></Marker>
      </GoogleMap>
    ));
    return (
      <div>
        <GoogleMapExample
          containerElement={
            <div
              id="map"
              style={{
                height: `700px`,
                width: "715px",
                marginLeft:'-0.5em',
                marginTop:'-0.5em'
              }}
            />
          }
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    set_pos: (value) => {
      dispatch(setpos(value));
    },
  };
};

export default connect(null, mapDispatchToProps)(MapContain);
