import React from 'react';

import {desadd, updatelocation } from '../redux'
// import {connect} from 'react-redux'

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
// import { useContext } from 'react';
// import { AppContext } from './Main'

import {createStore} from 'redux';
import { connect } from 'react-redux';
import { Icon,Button, Input, Segment, Label, Dropdown } from 'semantic-ui-react';








// const changeInputValue = (newValue) => {

//   dispatch({ type: 'UPDATE_LOCATION', data: newValue,});
// };

class SearchLocation extends React.Component {
    constructor(props) {
      super(props);
      this.state = { address: '',latlong:'', des_add:'' };
    }
    
   
    handleChange = address => {
      this.setState({ address });
      
    };

    
   
    handleSelect = address => {
      geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {console.log('Success', latLng)
        this.setState({
          latlong: latLng
        })
        // onChange={e => changeInputValue(e.target.value)}
        console.log("search",address);
        console.log("state",this.state.latlong);
        const value=this.state.latlong;
        console.log("value",value)
        this.props.set_value(this.state.latlong)
        console.log("print",this.props.set_value)
        console.log("destination address",address);
        this.setState({
          des_add:address
        })
        console.log("destination using state",this.state.des_add);
        this.props.set_des(this.state.des_add)
        console.log(this.props.set_des)
        // console.log(set_value);
      }
      // console.log("latlong",latLng)
      )
        
        .catch(error => console.error('Error', error));
        



    };
    
   
    render() {



      const like=this.state.latlong;
      // const {state, dispatch} = useContext(AppContext);
      

        
      

  
  //  console.log("Initial state",store.getState())
  //  const unsubscribe=store.subscribe(()=>console.log("updated state",store.getState()))
  //  store.dispatch(updatelocation())
  //  unsubscribe()
   
  
  


      return (
        <PlacesAutocomplete
          value={this.state.address}
          onChange={this.handleChange}
          // onChange={e => changeInputValue(e.target.value)}
          onSelect={this.handleSelect}
        >
          {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
            <div>
              <Segment raised color='black'>
   <Label color='red' ribbon className="label">
   DESTINATION
        </Label>
              <Input
            
                {...getInputProps({
                  placeholder: 'Add Final Destination',
                  icon:'map marker alternate',
                  size:'small',
                  iconPosition:'left',
                  className: 'location-search-input',
                })}
              />
              </Segment>
              {/* <Icon name='users'></Icon> */}
              <div className="autocomplete-dropdown-container">
                {loading && <div>Loading...</div>}
                {suggestions.map(suggestion => {
                  const className = suggestion.active
                    ? 'suggestion-item--active'
                    : 'suggestion-item';
                  // inline style for demonstration purpose
                  const style = suggestion.active
                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                  return (
                    <div
                      {...getSuggestionItemProps(suggestion, {
                        className,
                        style,
                      })}
                    >
                      {/* <Dropdown>{suggestion.description}</Dropdown> */}
                      <span>{suggestion.description}</span>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </PlacesAutocomplete>
        
      );
    }
  }

//  console.log(this.state.latLng);
  const mapDispatchToProps = (dispatch) => {

    //  console.log(set_value(value));

    return {
       set_value:(value)=>{
      //  console.log("aaja bhyii",value);
       dispatch(updatelocation(value))
       },
       set_des:(add)=>{
         dispatch(desadd(add))
       }

      // updatelocation : () => dispatch(this.state.latLng)
       
    }
  }

  
  // console.log(this.state.latlong);
  
   export default connect(null,mapDispatchToProps)(SearchLocation)  
  
  
  