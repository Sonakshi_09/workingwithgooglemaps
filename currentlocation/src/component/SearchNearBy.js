import React, { Component } from "react";
import MapContain from "./Map2";
import NearbySearch from "./nearbysearch";
import { connect } from "react-redux";
import axios from "axios";
import { Grid, Segment, Button, Label } from "semantic-ui-react";
import "./SearchNearBy.css";
import { token } from "../redux";

class SearchNearBy extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      datasave: [],
      datafetch: [],
    };
    this.fetchplace = this.fetchplace.bind(this);
  }
  saveplace() {
    console.log("TOKEN WALE KE ANDER HUN BHAI", this.props.token);
    var data = this.props.p_value;
    axios
      .post("http://localhost:9000/locations/saveplaces", data, {
        headers: { token: this.props.token },
      })
      .then(
        (response) => {
          console.log(response);
          alert("Places saved successfully");
        },
        (error) => {
          console.log(error);
          console.log("OOPS ITS THE ERROR PHASE.");
        }
      );
  }

  async fetchplace() {
    await this.setState({
      datasave: [],
    });
    var id = this.props.user;
    console.log("TOKEEEEN", this.props.token);

    axios
      .post("http://localhost:9000/locations/getplaces", null, {
        headers: { token: this.props.token },
      })
      .then((response) => {
        console.log(response);
        for (var i = 0; i < response.data.length; i++)
          this.state.datasave.push(response.data[i]);

        this.setState({
          datafetch: this.state.datasave,
        });
        console.log("DATA FETCH KI VALUE", this.state);
        console.log("SETSTATE KE ANDER KA DATA", this.state.datasave);
        console.log("DATA FETCH KI VALUE", this.state.datafetch);
      });
  }
  render() {
    return (
      <div>
        <div style={{ marginTop: "0.2em" }}>
          <NearbySearch></NearbySearch>
        </div>
        <Grid stackable columns={2}>
          <Grid.Column>
            <Segment className="first-segment">
              <MapContain></MapContain>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment className="headerText" raised secondary>
              <span
                style={{
                  fontWeight: "bold",
                  fontFamily: "serif",
                  fontSize: "18px",
                }}
              >
                Use these keywords to search nearby places :{" "}
              </span>
              <br />
              <Label.Group color="blue">
                <Label>cafe</Label>
                <Label>atm</Label>
                <Label>park</Label>
                <Label>restaurant</Label>
                <Label>school</Label>
                <Label>gym</Label>
                <Label>police</Label>
                <Label>spa</Label>
                <Label>car_wash</Label>
              </Label.Group>
            </Segment>
            <Grid stackable columns={2}>
              <Grid.Column>
                <Segment stacked className="saveplaceText">
                  <span
                    style={{
                      fontWeight: "bold",
                      fontFamily: "serif",
                      fontSize: "16px",
                    }}
                  >
                    Want to save your favourite nearby places? Easy Peezy!!!!{" "}
                    <br />
                    Tap{" "}
                    <span
                      style={{
                        fontWeight: "bolder",
                        fontFamily: "sans-serif",
                        color: "red",
                      }}
                    >
                      <i>SAVE SELECTED PLACES </i>
                    </span>{" "}
                    button, Select checkbox & Have all of them Saved !!{" "}
                  </span>
                </Segment>
                <Label
                  style={{
                    width: "320px",
                    height: "350px",
                    marginLeft: "2em",
                    marginTop: "2em",
                  }}
                  id="places"
                ></Label>
              </Grid.Column>
              <Grid.Column>
                <Segment stacked className="fetchplaceText">
                  <span
                    style={{
                      fontWeight: "bold",
                      fontFamily: "serif",
                      fontSize: "16px",
                    }}
                  >
                    Want to see previously stored places? You are all set to go
                    !!! <br />
                    Tap{" "}
                    <span
                      style={{
                        fontWeight: "bolder",
                        fontFamily: "sans-serif",
                        color: "red",
                      }}
                    >
                      <i>FETCH SAVED PLACES </i>
                    </span>
                    button and Have a look !
                  </span>
                </Segment>
                <div>
                  <ul>
                    {this.state.datasave.map((value) => {
                      return <Label>{value}</Label>;
                    })}
                  </ul>
                </div>
              </Grid.Column>
            </Grid>
            <Grid.Row>
              <Grid.Column>
                <ul>
                  <Button
                    floated="right"
                    className="fetchplace"
                    color="red"
                    type="submit"
                    onClick={() => this.fetchplace()}
                  >
                    FETCH SAVED PLACES
                  </Button>
                  &nbsp;&nbsp;{" "}
                  <Button
                    floated="left"
                    className="saveplace"
                    primary
                    type="submit"
                    onClick={() => this.saveplace()}
                  >
                    SAVE SELECTED PLACES
                  </Button>
                </ul>
              </Grid.Column>
            </Grid.Row>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const dataStateToProps = (state) => {
  console.log("State of token", state.token);
  return {
    p_value: state.place_val,
    token: state.token,
    user: state.id,
  };
};
export default connect(dataStateToProps, null)(SearchNearBy);
