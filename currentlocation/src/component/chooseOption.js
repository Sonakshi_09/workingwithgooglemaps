import React from "react";
import { Grid, Image } from "semantic-ui-react";
import { Icon, Input, Button, Segment, Label } from "semantic-ui-react";
import "./chooseOption.css";
import { Link } from "react-router-dom";
import Logout from "./logout";
import "./nearbysearch.css";

class ChooseOption extends React.Component {
  render() {
    return (
      <div>
        <div className="nearbyBackground">
          <Link to="/">
            <Logout />
          </Link>
        </div>
        <div className="background-design">
          <Grid>
            <Grid.Row>
              <Grid.Column width={2}></Grid.Column>
              <Grid.Column width={6}>
                <Segment raised>
                  <Label color="red" ribbon>
                    Addwaypoints
                  </Label>
                  AddWaypoints
                </Segment>
              </Grid.Column>
              <Grid.Column width={6}>
                <Segment raised>
                  <Label color="red" ribbon="right">
                    Search Nearby Places
                  </Label>
                  Search Nearby Places
                </Segment>
              </Grid.Column>
              <Grid.Column width={2}></Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={8}>
                <Segment className="addWayPointsTextDesign" raised>
                  <div className="insideAddWayPointsText">
                    <Label>Looking for places near your area?</Label>
                    <br />
                    <Label>Confused how to look for it?</Label>
                    <br />
                    <Label>Do not worry. Here is what you can do !</Label>
                    <br />
                    <Label>
                      Tap on{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        <i>Search Nearby Places</i>
                      </span>{" "}
                      button below.
                    </Label>
                    <br />
                    <Label>
                      With keywords available, search within any radius(in
                      metres).
                    </Label>
                    <br />
                    <Label>A list will appear on the screen.</Label>
                    <br />
                    <Label>
                      Select the ones you want to save for future references.
                    </Label>
                    <br />
                    <Label>
                      You can view all those saved routes with{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        <i>Fetch Saved Places feature.</i>
                      </span>
                    </Label>
                    <br />
                    <Label>Is it not Easy Peezy Lemon Squeezy !! </Label>
                    <br />
                    <Label>What are you waiting for? </Label>
                    <br />
                    <Label>
                      Start{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        EXPLORING{" "}
                      </span>
                      !!{" "}
                    </Label>
                  </div>
                </Segment>
              </Grid.Column>
              <Grid.Column width={8}>
                <Segment className="searchNearByTextDesign" raised>
                  <div className="insideNearbyText">
                    <Label>Looking for places near your area?</Label>
                    <br />
                    <Label>Confused how to look for it?</Label>
                    <br />
                    <Label>Do not worry. Here is what you can do !</Label>
                    <br />
                    <Label>
                      Tap on{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        <i>Search Nearby Places</i>
                      </span>{" "}
                      button below.
                    </Label>
                    <br />
                    <Label>
                      With keywords available, search within any radius(in
                      metres).
                    </Label>
                    <br />
                    <Label>A list will appear on the screen.</Label>
                    <br />
                    <Label>
                      Select the ones you want to save for future references.
                    </Label>
                    <br />
                    <Label>
                      You can view all those saved routes with{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        <i>Fetch Saved Places feature.</i>
                      </span>
                    </Label>
                    <br />
                    <Label>Is it not Easy Peezy Lemon Squeezy !! </Label>
                    <br />
                    <Label>What are you waiting for? </Label>
                    <br />
                    <Label>
                      Start{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        EXPLORING{" "}
                      </span>
                      !!{" "}
                    </Label>
                  </div>
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row className="button_center">
              {/* <Grid.Column width={4}>h</Grid.Column>
      <Grid.column width={8}> */}
              <Button.Group>
                <Link to="/addWaypoints">
                  <Button color="teal">AddWaypoints</Button>
                </Link>
                <Button.Or />
                <Link to="/searchNearby">
                  <Button color="dark brown">Search NearBy Places</Button>
                </Link>
              </Button.Group>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default ChooseOption;
