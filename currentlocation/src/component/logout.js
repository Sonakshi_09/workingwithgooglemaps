import React from "react";
import { GoogleLogout } from "react-google-login";
import "./logout.css";
const clientId =
  "111458893267-asfnr0bb67s3nifuhvl9e3drm0tssj1o.apps.googleusercontent.com";

function Logout() {
  const onSuccess = (res) => {
    alert("Logged out successfully!");
    console.log("Logout successful");
  };

  return (
    <div className="logoutDesign">
      <GoogleLogout
        className="logoutBtn"
        clientId={clientId}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      />
    </div>
  );
}

export default Logout;
