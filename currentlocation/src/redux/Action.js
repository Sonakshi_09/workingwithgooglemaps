import {ADD_WAYPOINT, UPDATE_LOCATION,SEND_ID, DES_ADD, ROUTE_DES, ROUTE_WAYPT, CHECK_STATUS, SAVED_ROUTE_DES, SAVED_ROUTES_WAYPTS, TOKEN,SETPOS,SETPLACE} from './ActionTypes'
export function updatelocation(like){
    console.log("like",like);
    return {
      type: UPDATE_LOCATION,
      info: "location selected",
      payload: like
    }
  };

  export function addwaypoint(point){
    console.log("points",point);
    return {

      type: ADD_WAYPOINT,
      info: "points added",
      payload: point
    }
  }
  
  export function sendid(id){
    console.log("id",id);
    return{
      type: SEND_ID,
      info:"sending id to backend",
      payload:id
    }
  }
  export function desadd(loc){
    console.log("loc",loc);
    return{
      type: DES_ADD,
      info: "sending curr loc",
      payload:loc
    }
  }
  
  export function checkstatus(value){
    console.log("des arr",value)
    return{
      type: CHECK_STATUS,
      info:"CHECKING",
      payload: value

    }
  }
  export function savedroutesdes(value){
    console.log("savedroutesdestination",value)
    return{
      type: SAVED_ROUTE_DES,
      info:"sending saved route destination",
      payload:value
    }
  }
  export function savedrouteswaypts(arr){
    console.log("saved_waypt_arr",arr)
    return{
      type:SAVED_ROUTES_WAYPTS,
      info:"sending waypt_arr",
      payload:arr
    }
  }
  export function token(value){
    console.log("token",value)
    return{
      type:TOKEN,
      info:'sending token',
      payload:value
    }
  }
  export function setpos(value){
    console.log("position",value)
    return{
      type:SETPOS,
      info:'sending position',
      payload:value

    }
  }
  export function setplace(value){
      console.log("places",value)
      return{
        type:SETPLACE,
        info:'sending places',
        payload:value
  
      }
  }