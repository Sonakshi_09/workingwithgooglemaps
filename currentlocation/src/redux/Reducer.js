import {ADD_WAYPOINT, UPDATE_LOCATION,SEND_ID,DES_ADD, ROUTE_DES, ROUTE_WAYPT, CHECK_STATUS, SAVED_ROUTE_DES, SAVED_ROUTES_WAYPTS,TOKEN,SETPOS,SETPLACE} from './ActionTypes'
const initialState={
    values : '',point: [],id:'',des_add:'',des_arr:[],check_status:'',saved_des:{},saved_waypts:{},token:'',pos:{}, place_val:{}
  }
  
  const reducer = (state=initialState,action)=> {
      console.log("Payload", action.payload);
    switch(action.type){
      case UPDATE_LOCATION: return {
        ...state,
        values: action.payload
      }
      case ADD_WAYPOINT: return {
        ...state,
        point: action.payload
      }
      case SEND_ID: return{
        ...state,
        id:action.payload
      }
      case DES_ADD: return{
        ...state,
        des_add:action.payload
      }
      case CHECK_STATUS:return{
        ...state,
        check_status:action.payload
        
      }
      case SAVED_ROUTE_DES:return{
        ...state,
        saved_des:action.payload
      }
      case SAVED_ROUTES_WAYPTS:return{
        ...state,
        saved_waypts:action.payload

      }
      case TOKEN:return{
        ...state,
        token:action.payload
      }
      case SETPOS:return{
        ...state,
        pos:action.payload
      }
      case SETPLACE:return{
        ...state,
        place_val:action.payload
      }
      default : return state
    }
  }
  


  export default reducer